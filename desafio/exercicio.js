var express = require ('express');
var path = require ('path');
var axios = require ('axios');
var app = express();

var getUsers = () => axios ({
method:  'GET',
hearders: {
    'Content-Type': 'application/json',
    'Accept': 'application/vdn.vtex.ds.v10=json'
},
 url : 'https://api.vtex.com/acupula/dataentities/AL/search',
})

app.get('/', async (req, res) => {
    
    try {
        var { data: users } = await getUsers()

        if (users) {
            res.send(users)
        }

    } catch (error) {
        console.log(error)
    }
});

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
